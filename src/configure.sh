#!/bin/bash

if [ "$(id -u)" != "0" ]; then
	echo "This command only works with a sudoer."
	exit 1	
fi

sudo apt-get -y install python-pip

sudo apt-get -y install python-dev

sudo apt-get install mongodb mongodb-clients mongodb-server

sudo pip install requests_toolbelt

sudo pip install requests

sudo pip install Flask

sudo pip install flask_restful

sudo pip install flask_httpauth

sudo pip install netifaces	

sudo pip install -U flask-cors

sudo pip install pymongo


