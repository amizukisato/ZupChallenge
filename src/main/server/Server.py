'''
Created on 20 de jul de 2017

@author: ailton
'''

from contextlib import closing
from flask import Flask,request
import socket
from netifaces import interfaces, ifaddresses, AF_INET
from flask_restful import  Api
from flask_cors.extension import CORS
from flask_cors.decorator import cross_origin
import main.pointer.DAOPointer as daoPointer
from bson.json_util import dumps


app = Flask(__name__, static_url_path='')
api = Api(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'




""" SERVICE PLACES """

@app.route('/places', methods=['GET','OPTIONS'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def getAllPlaces():
    return dumps(daoPointer.getAll())
    
@app.route('/places/search', methods=['GET','OPTIONS'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def searchPlaces():
    coordX = int(request.args.get('coordX'))
    coordY = int(request.args.get('coordY'))
    distance = int(request.args.get('distance'))
    return dumps(daoPointer.search(coordX, coordY, distance))


@app.route('/places', methods=['POST','OPTIONS'])
@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def createPlace():
    import json
    objectId = daoPointer.save(jsonData=json.loads(request.data))
    return dumps(daoPointer.get(objectId))
    
# Check if the interface and port is in use
def isFree(serverHost,serverPort ):
    
    connectionStatus = False
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        if sock.connect_ex((serverHost, serverPort)) == 0:
            print "Port in use"
            connectionStatus = False
        else:
            print "Port not in use"
            connectionStatus = True
    return connectionStatus


#List all avaliable network interfaces
def getHosts():
    serverHosts = []
    for ifaceName in interfaces():
        addresses = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, [{'addr':'No IP addr'}] )]
        for address in addresses: 
            if not str(address).startswith('No'):
                serverHosts.append( address)
                
    return serverHosts       


#Starts a flask server with default ip and default port
def start(serverHost = None, serverPort = 8000):
    if serverHost == None:
        hosts = getHosts()
        if len(hosts) > 1:
            serverHost = hosts[1]
        else:
            serverHost = hosts[0]
    
    while not isFree(serverHost, serverPort):
        serverPort = serverPort +1
        
   
    print "Running server in "+ serverHost + ":"+str(serverPort)
    app.run(host=serverHost, port=serverPort , threaded=True)
        