'''
Created on 19 de jul de 2017

@author: ailton
'''


from pymongo import MongoClient,GEO2D
from bson.son import SON
client = MongoClient('mongodb://127.0.0.1:27017')
database =  client.pointers

def createIndex():
    database.places.create_index([('location', GEO2D )] )

def save(pointer=None, jsonData=None):
    if jsonData != None:
        return database.places.insert(jsonData)
    return database.places.insert(pointer.toJson());

def getAll():
    return database.places.find()
    
def search(coordX, coordY, distance):
    query = {"location": SON([("$near", [coordX, coordY]), ("$maxDistance", distance)])}
    return database.places.find(query)

def deleteAll():
    return database.places.remove()

def get(objectId):
    try:
        return database.places.find({'_id':objectId})[0]
    except:
        return None