'''
Created on 20 de jul de 2017

@author: ailton
'''


import main.pointer.DAOPointer as daoPointer
from main.pointer.Pointer import Pointer

#starts the database

def init():
    daoPointer.createIndex()
    pointerLanchonete = Pointer('Lanchonete', 27, 12)
    pointerPosto = Pointer('Posto', 31, 18)
    pointerJoalheria = Pointer('Joalheria', 15, 12)
    pointerFloricultura = Pointer('Floricultura', 19, 21)
    pointerPub = Pointer('Pub', 12, 8)
    pointerSupermercado = Pointer('Supermercado', 23, 6)
    pointerChurrascaria = Pointer('Churrascaria', 28, 2)
    daoPointer.save(pointerLanchonete)
    daoPointer.save(pointerPosto)
    daoPointer.save(pointerJoalheria)
    daoPointer.save(pointerFloricultura)
    daoPointer.save(pointerPub)
    daoPointer.save(pointerSupermercado)
    daoPointer.save(pointerChurrascaria)

    
