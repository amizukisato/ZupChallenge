'''
Created on 19 de jul de 2017

@author: ailton
'''

class Pointer(object):
    '''
    classdocs
    '''
    

    def __init__(self, name, x, y ):
        self.name = name
        self.coordX = x
        self.coordY = y
    
        
    def toJson(self):
        return {
        'name': self.name,
        'location': [ self.coordX, self.coordY ] 
        
    }; 