'''
Created on 20 de jul de 2017

@author: ailton
'''
import unittest

import main.pointer.DAOPointer as daoPointer
import main.pointer.InitDatabase as initDatabase
from main.pointer.Pointer import Pointer


class TestPointer(unittest.TestCase):
    
    
    """ Init database"""
    @classmethod
    def setUpClass(cls):
        daoPointer.deleteAll()
        
    
    def setUp(self):
        initDatabase.init()
        

    def tearDown(self):
        daoPointer.deleteAll()
        

    def testPointerCreate(self):
        pointer = Pointer("teste", 100, 111)
        objectId = daoPointer.save(pointer)
        pointerRetrieved =  daoPointer.get(objectId)
        self.assertEquals(pointerRetrieved['name'], pointer.name)
        self.assertEquals(pointerRetrieved['location'], [100, 111])
        
        
    def testPointerGetAll(self):
        allPointers =  list(daoPointer.getAll())
        firstNameInList = 'Lanchonete'
        firstCoordinatesInList = [27, 12]
        sizeOfTHeList = 7
        self.assertEqual(sizeOfTHeList, len(allPointers) )
        self.assertEqual(firstNameInList, allPointers[0]['name'])
        self.assertEqual(firstCoordinatesInList, allPointers[0]['location'])
        
    def testPointerSearch(self):
        pointers = list(daoPointer.search(coordX = 20,coordY= 10, distance=10)   )         
        firstNameInList = 'Supermercado'
        firstCoordinatesInList = [23, 6]
        sizeOfTheList = 4
        self.assertEqual(sizeOfTheList, len(pointers) )
        self.assertEqual(firstNameInList, pointers[0]['name'])
        self.assertEqual(firstCoordinatesInList, pointers[0]['location'])
        
            
            
            
        
        