'''
Created on 20 de jul de 2017

@author: ailton
'''
import unittest

import main.pointer.DAOPointer as daoPointer
import main.pointer.InitDatabase as initDatabase
import main.server.Server as server
import requests


class TestServer(unittest.TestCase):
    
    
    
    
    """ Init database"""
    @classmethod
    def setUpClass(cls):
        daoPointer.deleteAll()
        cls.address = "http://"+server.getHosts()[1]+":8000"
        cls.session =  requests.Session()
    
    def setUp(self):
        initDatabase.init()
        

    def tearDown(self):
        daoPointer.deleteAll()
        

    def testServerCreate(self):
        import json
        data = {
            'name': 'Choperia',
            'location': [120, 170]
        }
        
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        newPointer = self.session.post(self.address +"/places", headers = headers,data=json.dumps(data)).json()
        self.assertEquals(newPointer['name'], 'Choperia')
        self.assertEquals(newPointer['location'],[120, 170])
        
    def testServerGetAll(self):
        pointers = self.session.get(self.address + "/places").json()
        sizeOfTHeList = 7
        firstNameInList = 'Lanchonete'
        firstCoordinatesInList = [27, 12]
        self.assertEqual(sizeOfTHeList, len(pointers) )
        self.assertEqual(firstNameInList, pointers[0]['name'])
        self.assertEqual(firstCoordinatesInList, pointers[0]['location'])
        
        
    def testServerSearch(self):
        pointers = self.session.get(self.address + "/places/search?coordX=20&coordY=10&distance=10").json()
        firstNameInList = 'Supermercado'
        firstCoordinatesInList = [23, 6]
        sizeOfTheList = 4
        self.assertEqual(sizeOfTheList, len(pointers) )
        self.assertEqual(firstNameInList, pointers[0]['name'])
        self.assertEqual(firstCoordinatesInList, pointers[0]['location'])
            
            
        
        