'''
Created on 20 de jul de 2017

@author: ailton
'''

import unittest
from test.pointer.TestPointer import TestPointer
from test.server.TestServer import TestServer

def buildSuiteClasses():
    testClasses = []
    testClasses.append(TestPointer) 
    testClasses.append(TestServer)
    return testClasses
    


def suite():
  
    suiteListClasses = buildSuiteClasses()
    suiteList = []
    loader = unittest.TestLoader()
   
    for testClass in suiteListClasses:
        suite = loader.loadTestsFromTestCase(testClass)
        suiteList.append(suite)
        
    big_suite = unittest.TestSuite(suiteList)
    return big_suite

 
def run_test():
    runner = unittest.TextTestRunner(verbosity=2)
    test_suite = suite()
    runner.run (test_suite) 

    