'''
Created on 19 de jul de 2017

@author: ailton
'''


import sys, traceback


try:
    if len(sys.argv) > 1:
        
        if sys.argv[1] == "--database" or sys.argv[1] == "-d" : 
            import main.pointer.InitDatabase as initDatabase
            initDatabase.init()
        if sys.argv[1] == "--publish" or sys.argv[1] == "-p" : 
            import main.server.Server as server
            server.start()
        if sys.argv[1] == "--test" or sys.argv[1] == "-t": 
            import test.SuiteTest as test
            test.run_test()
        if sys.argv[1] == "--help" or sys.argv[1] == "-h" : 
            print "comands python run.py [Options]"
            print "-d or --database | for initialize the database"
            print "-p or --publish  | for initialize and publish the server"
            print "-h or --help     | for show this menu"
            print "-t or --test     | for run all unit tests"
           
            
            
            
            
except:
    traceback.print_exc()
    pass






if __name__ == '__main__':
    pass