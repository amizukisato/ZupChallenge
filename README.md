Welcome to the XYZInc

This document explains how to run and configure the challenge.


1. First you need to install all dependencies that is needed from the software

    a) mongodb
    
    b) python-dev
    
    c) pip
    
    d) Flask
    
    e) requests-toolbelt
    
    f) pymongo
    
    g) Flask-CORS
    
    The shell script configure.sh do this automatically
    ```
        $ sh configure.sh
    ```


2. You can create database example using

    ```
    $ python run.py -d
    
    or
    
    $ python run.py --database 
    ```

    

3. For run the server to listen HTTP requests
    ```
    $ python run.py -p
    
    or
    
    $ python run.py --publish
    ```


4. After all dependencies has been configured and the server has been published ,  the you can run the all the unit tests, to check if is everything ok.
    ```
    $ python run.py -t
    
        or
    
    $ python run.py --test
    ```

5. To see all commands 
    ```
    $ python run.py -h
    
        or
    
    $ python run.py --help
    ```